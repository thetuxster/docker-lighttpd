FROM centos
MAINTAINER Joseph Jones
EXPOSE 8080
CMD [ "/usr/sbin/lighttpd", "-f", "/etc/lighttpd/lighttpd.conf", "-D" ]
RUN yum install -y epel-release
RUN yum install -y lighttpd
VOLUME [ "/mirrors", "/var/log/lighttpd" ]
WORKDIR /mirrors
ADD lighttpd.conf /etc/lighttpd/lighttpd.conf
